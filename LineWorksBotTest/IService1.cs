﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace LineWorksBotTest
{
    // メモ: [リファクター] メニューの [名前の変更] コマンドを使用すると、コードと config ファイルの両方で同時にインターフェイス名 "IService1" を変更できます。
    [ServiceContract]
    public interface IService1
    {

        [WebInvoke(Method = "POST", UriTemplate = "callback", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json)]
        void callback(string msgType, string serviceId, string account, string writerDomainId, string writerUserNo, string botNo, string channelNo, string createdTime, string content, string extras, string msgTypeCode);

        [WebInvoke(Method = "POST", UriTemplate = "LineCallBack", RequestFormat = WebMessageFormat.Json)]
        void lineCallBack(LineEventObject lineEventObject);

        // TODO: ここにサービス操作を追加します。
    }





    //LineWorksAPI
    [DataContract]    
    public class Image
    {
        [DataMember]
        public int botNo { get; set; }
        [DataMember]
        public string channelNo { get; set; }
        [DataMember]
        public string type { get; set; }
        [DataMember]
        public Content content { get; set; }
    }
    [DataContract]  
    public class Content
    {
        [DataMember]
        public string previewUrl { get; set; }
        [DataMember]
        public string resourceUrl { get; set; }
        [DataMember]
        public int width { get; set; }
        [DataMember]
        public int height { get; set; }
    }








    //LineAPI

    // サービス操作に複合型を追加するには、以下のサンプルに示すようにデータ コントラクトを使用します。
    [DataContract]
    public class LineEventObject
    {

        [DataMember(Name = "events")]
        public List<Events> events { get; set; }

    }

    [DataContract]
    public class Events
    {

        [DataMember(Name = "replyToken")]
        public string replyToken { get; set; }

        [DataMember(Name = "type")]
        public string type { get; set; }

        [DataMember(Name = "timestamp")]
        public string timestamp { get; set; }

        [DataMember(Name = "source")]
        public Source source { get; set; }

        [DataMember(Name = "message")]
        public Message message { get; set; }

        [DataMember(Name = "beacon")]
        public Beacon beacon { get; set; }

    }

    [DataContract]
    public class Beacon
    {
        [DataMember(Name = "hwid")]
        public string hwid { get; set; }
        [DataMember(Name = "type")]
        public string type { get; set; }
    }
    [DataContract]
    public class Source
    {
        [DataMember(Name = "type")]
        public string type { get; set; }

        [DataMember(Name = "userId")]
        public string userId { get; set; }

        [DataMember(Name = "groupId")]
        public string groupId { get; set; }

        [DataMember(Name = "roomId")]
        public string roomId { get; set; }
    }


    [DataContract]
    public class Message
    {
        [DataMember(Name = "id")]
        public string id { get; set; }

        [DataMember(Name = "type")]
        public string type { get; set; }

        [DataMember(Name = "text")]
        public string text { get; set; }

        [DataMember(Name = "groupId")]
        public string groupId { get; set; }

        [DataMember(Name = "originalContentUrl")]
        public string originalContentUrl { get; set; }

        [DataMember(Name = "previewImageUrl")]
        public string previewImageUrl { get; set; }

    }






    [DataContract]
    public class wether
    {
         [DataMember]
         public string cod { get; set; }
         [DataMember]
         public float message { get; set; }
         [DataMember]
         public int cnt { get; set; }
         [DataMember]
         public List[] list { get; set; }
         [DataMember]
         public City city { get; set; }
    }
    [DataContract]
    public class City
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public Coord coord { get; set; }
        [DataMember]
        public string country { get; set; }
    }
    [DataContract]
    public class Coord
    {
        [DataMember]
        public float lat { get; set; }
        [DataMember]
        public float lon { get; set; }
    }
    [DataContract]
    public class List
    {
        [DataMember]
        public int dt { get; set; }
        [DataMember]
        public Main main { get; set; }
        [DataMember]
        public Weather[] weather { get; set; }
        [DataMember]
        public Clouds clouds { get; set; }
        [DataMember]
        public Wind wind { get; set; }
        [DataMember]
        public Rain rain { get; set; }
        [DataMember]
        public Sys sys { get; set; }
        [DataMember]
        public string dt_txt { get; set; }
    }
    [DataContract]
    public class Main
    {
        [DataMember]
        public float temp { get; set; }
        [DataMember]
        public float temp_min { get; set; }
        [DataMember]
        public float temp_max { get; set; }
        [DataMember]
        public float pressure { get; set; }
        [DataMember]
        public float sea_level { get; set; }
        [DataMember]
        public float grnd_level { get; set; }
        [DataMember]
        public int humidity { get; set; }
        [DataMember]
        public float temp_kf { get; set; }
    }
    [DataContract]
    public class Clouds
    {
        [DataMember]
        public int all { get; set; }
    }
    [DataContract]
    public class Wind
    {
        [DataMember]
        public float speed { get; set; }
        [DataMember]
        public float deg { get; set; }
    }
    [DataContract]
    public class Rain
    {
        [DataMember]
        public float _3h { get; set; }
    }
    [DataContract]
    public class Sys
    {
        [DataMember]
        public string pod { get; set; }
    }
    [DataContract]
    public class Weather
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string main { get; set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public string icon { get; set; }
    }




}
