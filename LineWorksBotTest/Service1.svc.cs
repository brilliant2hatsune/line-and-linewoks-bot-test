﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using メール通知奴;

namespace LineWorksBotTest
{
    // メモ: [リファクター] メニューの [名前の変更] コマンドを使用すると、コード、svc、および config ファイルで同時にクラス名 "Service1" を変更できます。
    // 注意: このサービスをテストするために WCF テスト クライアントを起動するには、ソリューション エクスプローラーで Service1.svc または Service1.svc.cs を選択し、デバッグを開始してください。
    public class Service1 : IService1
    {
        public void callback(string msgType, string serviceId, string account, string writerDomainId, string writerUserNo, string botNo, string channelNo, string createdTime, string content, string extras, string msgTypeCode)
        {
            Logger.log("BotTest起動");

           
            Logger.log(msgType + serviceId + account + writerDomainId + writerUserNo + botNo + channelNo + createdTime + content + extras + msgTypeCode);

            var serializer = new JavaScriptSerializer();
            string json;
            StringContent content1;
            HttpResponseMessage response;
                
            var client = new HttpClient();

            client.DefaultRequestHeaders.Add("Authorization", "Bearer AAAA8tHufL/vxBvLUrOgfCtqEbBuaesMitSwPnqeMvsdj3KtESJ9Y5Fftwu20KKe7kCoeinBaMfRpdCL6pyyE8yjI/agKtB/cLJ+SehNzdvz9qagfneuepDicu1YeuX8LJ85tn2vHpYnxD0lIKeHAVyx6fwk+JT7S9zjuODbLn4qDtKivcLt+XEtB1bHEYQYtNhdOi7zjLlsIrfyNKAV2ywIOc6c5YzCxANGyWVszStS8DeaW8Lw6M2SpF9fLtK5FaQB4ptdYv1Uwuf+r2P7v2ZV8Rq/f/J8thswhMPGdqGSnex7o8qjrh5Wo1ZNVwa0+Zx2oQt3+vbNBJk9oeYTHuJFBLA= ");
            client.DefaultRequestHeaders.Add("consumerKey", "cuGJ0CLdcGlxD5EioW7g");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var param = new Hashtable();
            param["botNo"] = 537;                           // 数値型のパラメータ
            param["channelNo"] = channelNo;
            param["type"] = "text";

            //param["content"] = "ぼっとから返信\n" + content;
            if ((content.Contains("からだ") || content.Contains("体")) && (content.Contains("すこやか") || content.Contains("健やか")) && (content.Contains("茶") || content.Contains("ちゃ")))
            {
                param["content"] = "だぶるぅ～♪";
                
              
                json = serializer.Serialize(param);
                content1 = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

                response = client.PostAsync("https://apis.worksmobile.com/jp1DewSLhNoCT/message/sendMessage", content1).Result;
                Logger.log(response.ToString());
            }
            else if (content.Contains("天気") || content.Contains("てんき"))
            {

                WebClient wc = new WebClient();
                using (Stream st = wc.OpenRead("http://api.openweathermap.org/data/2.5/forecast?APPID=9d1ba8d978825723d0d14922d43ef9e2&id=1848354&units=metric"))
                {
                    var wetherSerializer = new DataContractJsonSerializer(typeof(wether));
                    wether info = (wether)wetherSerializer.ReadObject(st);
                    Logger.log(info.city.name);
                    Logger.log(info.list[0].main.temp_min.ToString());
                    Logger.log(info.list[0].main.temp_max.ToString());
                    Logger.log(info.list[0].weather[0].main);
                    Logger.log(info.list[0].weather[0].icon);

                    byte[] pic = wc.DownloadData("http://openweathermap.org/img/w/" + info.list[0].weather[0].icon + ".png");
                    using (MemoryStream img = new MemoryStream(pic))
                    {
                        Bitmap bmp = new Bitmap(img);
                        string mappath = HttpContext.Current.Server.MapPath("./");
                        Logger.log(mappath);
                        if (!Directory.Exists(mappath + "\\img")) ;
                        Directory.CreateDirectory(mappath + "\\img");
                        bmp.Save(mappath + "\\img\\" + info.list[0].weather[0].icon + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

                    }

                    param["content"] = "続きまして横浜のお天気です。";

                    json = serializer.Serialize(param);
                    content1 = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

                    response = client.PostAsync("https://apis.worksmobile.com/jp1DewSLhNoCT/message/sendMessage", content1).Result;
                    Logger.log("続きまして" + response.ToString());


                    //非同期なので待たないと順番がへんになる　WebClientのほうがいいな。
                    System.Threading.Thread.Sleep(1000);

                    var path = HttpRuntime.AppDomainAppVirtualPath;

                    Logger.log("https://kubotty.moe.hm" + path + "/img/" + info.list[0].weather[0].icon + ".jpg");
                    Image sndImage = new Image();
                    sndImage.botNo = 537;
                    sndImage.type = "image";
                    sndImage.channelNo = channelNo;
                    sndImage.content = new Content();
                    sndImage.content.previewUrl = "https://kubotty.moe.hm" + path + "/img/" + info.list[0].weather[0].icon + ".jpg";
                    sndImage.content.resourceUrl = "https://kubotty.moe.hm" + path + "/img/" + info.list[0].weather[0].icon + ".jpg";

                    json = serializer.Serialize(sndImage);
                    Logger.log(json);
                    content1 = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

                    response = client.PostAsync("https://apis.worksmobile.com/jp1DewSLhNoCT/message/sendMessage", content1).Result;
                    Logger.log(response.ToString());
                    //非同期なので待たないと順番がへんになる　WebClientのほうがいいな。
                    System.Threading.Thread.Sleep(1000);


                    if (info.list[0].rain == null) { info.list[0].rain = new Rain(); }
                    if (info.list[1].rain == null) { info.list[1].rain = new Rain(); }
                    if (info.list[2].rain == null) { info.list[2].rain = new Rain(); }


                    TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("Tokyo Standard Time");

                    DateTime UNIX_EPOCH = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

                    DateTime utc = UNIX_EPOCH.AddSeconds(info.list[0].dt);
                    DateTime jst2 = TimeZoneInfo.ConvertTimeFromUtc(utc, tzi);
                    Logger.log("UTC: " + utc.ToString("G") + " -> JST: " + jst2.ToString("G"));

                    param["content"] = jst2.TimeOfDay + "\n気温(℃):" + info.list[0].main.temp + "\n湿度(%):" + info.list[0].main.humidity + "\n風速(m/s):" + info.list[0].wind.speed + "\n降水量(mm):" + info.list[0].rain._3h + "\n天気詳細:" + info.list[0].weather[0].description;


                    utc = UNIX_EPOCH.AddSeconds(info.list[1].dt);
                    jst2 = TimeZoneInfo.ConvertTimeFromUtc(utc, tzi);
                    param["content"] += "\n\n" + jst2.TimeOfDay + "\n気温(℃):" + info.list[1].main.temp + "\n湿度(%):" + info.list[1].main.humidity + "\n風速(m/s):" + info.list[1].wind.speed + "\n降水量(mm):" + info.list[1].rain._3h + "\n天気詳細:" + info.list[1].weather[0].description;

                    utc = UNIX_EPOCH.AddSeconds(info.list[2].dt);
                    jst2 = TimeZoneInfo.ConvertTimeFromUtc(utc, tzi);
                    param["content"] += "\n\n" + jst2.TimeOfDay + "\n気温(℃):" + info.list[2].main.temp + "\n湿度(%):" + info.list[2].main.humidity + "\n風速(m/s):" + info.list[2].wind.speed + "\n降水量(mm):" + info.list[2].rain._3h + "\n天気詳細:" + info.list[2].weather[0].description;

                    json = serializer.Serialize(param);
                    content1 = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
                    response = client.PostAsync("https://apis.worksmobile.com/jp1DewSLhNoCT/message/sendMessage", content1).Result;
                    Logger.log("天気情報" + response.ToString());

                    //非同期なので待たないと順番がへんになる　WebClientのほうがいいな。
                    System.Threading.Thread.Sleep(1000);

                    param["content"] = "以上Death！";
                    json = serializer.Serialize(param);
                    content1 = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

                    response = client.PostAsync("https://apis.worksmobile.com/jp1DewSLhNoCT/message/sendMessage", content1).Result;

                }
            }
            else
            {
                param["content"] = content + "じゃなくて、「てんき」と入力してみてください";

                json = serializer.Serialize(param);
                content1 = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

                response = client.PostAsync("https://apis.worksmobile.com/jp1DewSLhNoCT/message/sendMessage", content1).Result;
                Logger.log(response.ToString());
            }





            
            
        }


        
        public void lineCallBack(LineEventObject lineEventObject)
        {
            Logger.log("LineBotTest起動");

            foreach(Events evt in lineEventObject.events){
                Logger.log(evt.timestamp + ":" + evt.type + ":" + evt.source.userId);



                if (evt.type == "beacon") {
                    Logger.log(evt.beacon.hwid);
                    Logger.log(evt.beacon.type);
                    
                    var client = new HttpClient();

                    client.DefaultRequestHeaders.Add("Authorization", "Bearer Iuu8c3FMjGD2afk8T6F/qJH8PCmar5Fq5oMZtDWdbmCIhUATue8Idr6SbE+o7Z9OOxitOTxB3S2tRxhxb2JLU+61ewAkKdVZtgXcbbrOZDtKGT+fAOcS0lVPVWkCtrTKxO9p3Bb38HH7VhflJqidxQdB04t89/1O/w1cDnyilFU= ");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var param = new Hashtable();
                    param["replyToken"] = evt.replyToken;

                    var serializer = new JavaScriptSerializer();
                    string json;
                    string url = "https://api.line.me/v2/bot/message/reply";
                    StringContent content1;
                    HttpResponseMessage response;
                    Message msg1 = new Message();
                    Message msg2 = new Message();
                    
                    /*
                    if (evt.beacon.type == "enter")
                        msg.text = "あなたはビーコンID：" + evt.beacon.hwid + "の範囲に入りました。\nUserID:" + evt.source.userId;
                    else if (evt.beacon.type == "leave")
                        msg.text = "あなたはビーコンID：" + evt.beacon.hwid + "の範囲から出ました。UserID:" + evt.source.userId;
                    else 
                        msg.text = "あなたはビーコンID：" + evt.beacon.hwid + "の広告バナーをタップしました。UserID:" + evt.source.userId;;
                    */

                    if (evt.beacon.type == "enter") {

                        DateTime dt = System.DateTime.Now.AddHours(1);

                        //msg1.type = "text";
                        //msg1.text = "○×店へようこそ！\n本日" + dt.ToString("tthh時")  + "以降に来店でご利用できる、お得なクーポンを配布します！";
                        msg2.type = "image";
                        var path = HttpRuntime.AppDomainAppVirtualPath;
                        Logger.log("https://kubotty.moe.hm" + path + "/img/coupon.jpg");
                        msg2.originalContentUrl = "https://kubotty.moe.hm" + path + "/img/coupon.jpg";
                        msg2.previewImageUrl = "https://kubotty.moe.hm" + path + "/img/coupon.jpg";


                    }
                    param["messages"] = new Message[] {  msg2 };
                    json = serializer.Serialize(param);
                    Logger.log(url);
                    Logger.log(json);

                    content1 = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

                    response = client.PostAsync(url, content1).Result;
                    Logger.log(response.ToString());
                }
                else if (evt.type == "message")
                {

                    Logger.log(evt.message.text);

                    var client = new HttpClient();

                    client.DefaultRequestHeaders.Add("Authorization", "Bearer Iuu8c3FMjGD2afk8T6F/qJH8PCmar5Fq5oMZtDWdbmCIhUATue8Idr6SbE+o7Z9OOxitOTxB3S2tRxhxb2JLU+61ewAkKdVZtgXcbbrOZDtKGT+fAOcS0lVPVWkCtrTKxO9p3Bb38HH7VhflJqidxQdB04t89/1O/w1cDnyilFU= ");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var param = new Hashtable();
                    param["replyToken"] = evt.replyToken;

                    var serializer = new JavaScriptSerializer();
                    string json;
                    string url = "https://api.line.me/v2/bot/message/reply";
                    StringContent content1;
                    HttpResponseMessage response;
                    Message msg = new Message();
                    msg.type = "text";
                    msg.text = "ぼっとから返信\n" + evt.message.text;
                    if ((evt.message.text.Contains("からだ") || evt.message.text.Contains("体")) && (evt.message.text.Contains("すこやか") || evt.message.text.Contains("健やか")) && (evt.message.text.Contains("茶") || evt.message.text.Contains("ちゃ")))
                    {
                        msg.text = "だぶるぅ～♪";
                        param["messages"] = new Message[] { msg };
                    }

                    else if (evt.message.text.Contains("帰れ") || evt.message.text.Contains("かえれ") || evt.message.text.Contains("消えろ") || evt.message.text.Contains("きえろ") || evt.message.text.Contains("死ね") || evt.message.text.Contains("しね") || evt.message.text.Contains("され") || evt.message.text.Contains("去れ"))
                    {

                        //https://api.line.me/v2/bot/group/{groupId}/leave
                        switch (evt.source.type)
                        {
                            case "group":
                                url = "https://api.line.me/v2/bot/group/" + evt.source.userId + "/leave";
                                break;
                            case "room":
                                url = "https://api.line.me/v2/bot/room/" + evt.source.roomId + "/leave";
                                break;

                            default:
                                return;

                        }

                    }
                    else if (evt.message.text.Contains("天気") || evt.message.text.Contains("てんき"))
                    {

                        WebClient wc = new WebClient();

                        using (Stream st = wc.OpenRead("http://api.openweathermap.org/data/2.5/forecast?APPID=9d1ba8d978825723d0d14922d43ef9e2&id=1848354&units=metric"))
                        {
                            var wetherSerializer = new DataContractJsonSerializer(typeof(wether));
                            wether info = (wether)wetherSerializer.ReadObject(st);
                            Logger.log(info.city.name);
                            Logger.log(info.list[0].main.temp_min.ToString());
                            Logger.log(info.list[0].main.temp_max.ToString());
                            Logger.log(info.list[0].weather[0].main);
                            Logger.log(info.list[0].weather[0].icon);

                            byte[] pic = wc.DownloadData("http://openweathermap.org/img/w/" + info.list[0].weather[0].icon + ".png");
                            using (MemoryStream img = new MemoryStream(pic))
                            {
                                Bitmap bmp = new Bitmap(img);
                                string mappath = HttpContext.Current.Server.MapPath("./");
                                Logger.log(mappath);
                                if (!Directory.Exists(mappath + "\\img")) ;
                                Directory.CreateDirectory(mappath + "\\img");
                                bmp.Save(mappath + "\\img\\" + info.list[0].weather[0].icon + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

                            }

                            msg.text = "続きまして横浜のお天気です。";

                            Message msg2 = new Message();
                            msg2.type = "image";

                            var path = HttpRuntime.AppDomainAppVirtualPath;

                            Logger.log("https://kubotty.moe.hm" + path + "/img/" + info.list[0].weather[0].icon + ".jpg");

                            msg2.originalContentUrl = "https://kubotty.moe.hm" + path + "/img/" + info.list[0].weather[0].icon + ".jpg";
                            msg2.previewImageUrl = "https://kubotty.moe.hm" + path + "/img/" + info.list[0].weather[0].icon + ".jpg";


                            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("Tokyo Standard Time");

                            DateTime UNIX_EPOCH = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

                            DateTime utc = UNIX_EPOCH.AddSeconds(info.list[0].dt);
                            DateTime jst2 = TimeZoneInfo.ConvertTimeFromUtc(utc, tzi);
                            Logger.log("UTC: " + utc.ToString("G") + " -> JST: " + jst2.ToString("G"));

                            if (info.list[0].rain == null) { info.list[0].rain = new Rain(); }
                            if (info.list[1].rain == null) { info.list[1].rain = new Rain(); }
                            if (info.list[2].rain == null) { info.list[2].rain = new Rain(); }
                            Logger.log(info.list[0].main.temp.ToString());
                            Logger.log(info.list[0].main.humidity);
                            Logger.log(info.list[0].wind.speed.ToString());
                            Logger.log(info.list[0].rain._3h.ToString());

                            Logger.log(info.list[0].weather[0].description);
                            Message msg3 = new Message();
                            msg3.type = "text";
                            msg3.text = jst2.TimeOfDay + "\n気温(℃):" + info.list[0].main.temp + "\n湿度(%):" + info.list[0].main.humidity + "\n風速(m/s):" + info.list[0].wind.speed + "\n降水量(mm):" + info.list[0].rain._3h + "\n天気詳細:" + info.list[0].weather[0].description;
                            Logger.log(msg3.text);

                            utc = UNIX_EPOCH.AddSeconds(info.list[1].dt);
                            jst2 = TimeZoneInfo.ConvertTimeFromUtc(utc, tzi);
                            msg3.text += "\n\n" + jst2.TimeOfDay + "\n気温(℃):" + info.list[1].main.temp + "\n湿度(%):" + info.list[1].main.humidity + "\n風速(m/s):" + info.list[1].wind.speed + "\n降水量(mm):" + info.list[1].rain._3h + "\n天気詳細:" + info.list[1].weather[0].description;
                            Logger.log(msg3.text);

                            utc = UNIX_EPOCH.AddSeconds(info.list[2].dt);
                            jst2 = TimeZoneInfo.ConvertTimeFromUtc(utc, tzi);
                            msg3.text += "\n\n" + jst2.TimeOfDay + "\n気温(℃):" + info.list[2].main.temp + "\n湿度(%):" + info.list[2].main.humidity + "\n風速(m/s):" + info.list[2].wind.speed + "\n降水量(mm):" + info.list[2].rain._3h + "\n天気詳細:" + info.list[2].weather[0].description;
                            Logger.log(msg3.text);

                            Message msg4 = new Message();
                            msg4.type = "text";
                            msg4.text = "以上Death！";

                            Logger.log(msg4.text);

                            param["messages"] = new Message[] { msg, msg2, msg3, msg4 };

                        }
                        //return;                    

                    }
                    else { return; }

                    json = serializer.Serialize(param);
                    Logger.log(url);
                    Logger.log(json);

                    content1 = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

                    response = client.PostAsync(url, content1).Result;
                    Logger.log(response.ToString());

                }
            }
            
        }
     
    }
 
}
